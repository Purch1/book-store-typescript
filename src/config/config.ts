import dotenv from 'dotenv';

dotenv.config();

const MONGO_URL = process.env.MONGODB_URI;

const SERVER_PORT = process.env.SERVER_PORT ? Number(process.env.SERVER_PORT) : 1387;

export const config = {
    mongo: {
        url: MONGO_URL
    },
    server: {
        port: SERVER_PORT
    }
};